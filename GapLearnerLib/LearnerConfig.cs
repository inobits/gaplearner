﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace GapLearnerLib
{


    class LearnerConfig
    {
        public static IConfigurationRoot Configuration { get; set; }

        public IConfigurationRoot LoadConfig()
        {
            var builder = new ConfigurationBuilder()
             .AddJsonFile($"appsettings.json", true, true);

            Configuration = builder.Build();
            return Configuration;

            //Console.WriteLine($"option1 = {Configuration["option1"]}");
            //Console.WriteLine($"option2 = {Configuration["option2"]}");
            //Console.WriteLine(
            //    $"suboption1 = {Configuration["subsection:suboption1"]}");
            //Console.WriteLine();

            //Console.WriteLine("Wizards:");
            //Console.Write($"{Configuration["wizards:0:Name"]}, ");
            //Console.WriteLine($"age {Configuration["wizards:0:Age"]}");
            //Console.Write($"{Configuration["wizards:1:Name"]}, ");
            //Console.WriteLine($"age {Configuration["wizards:1:Age"]}");
        }
    }
}
