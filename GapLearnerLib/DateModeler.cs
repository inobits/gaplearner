﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace GapLearnerLib
{
    public class DateModeler
    {
        public static IConfigurationRoot Configuration { get; set; }

        public DateModeler()
        {
            var configTest = new LearnerConfig();
            Configuration = configTest.LoadConfig();
        }

        public DateCollection ClassifyDates(List<DateTime> input)
        {
            var ClassifiedDates = new DateCollection();
            foreach (var item in input)
            {
                ClassifiedDates.Dates.Add(new DateModel() { TheDateTime = item });
            }

           

            ClassifiedDates = CalculateDateDistanceFromStart(ClassifiedDates);
            if (ClassifiedDates.DetectedPattern == DateCollection.DatePattern.StartOfMonth)
            {
                return ClassifiedDates;
            }

            ClassifiedDates = CalculateDateDistanceFromEnd(ClassifiedDates);
            if (ClassifiedDates.DetectedPattern == DateCollection.DatePattern.EndOfMonth)
            {
                return ClassifiedDates;
            }

            ClassifiedDates = CalculateDateDistanceFromNthWeekday(ClassifiedDates);
            if (ClassifiedDates.DetectedPattern == DateCollection.DatePattern.EveryNthWeekDayOfMonth)
            {
                return ClassifiedDates;
            }

            ClassifiedDates = CalculateDateDistanceFromLastNthWeekday(ClassifiedDates);
            if (ClassifiedDates.DetectedPattern == DateCollection.DatePattern.LastNthWeekDayOfMonth)
            {
                return ClassifiedDates;
            }


            // No easy pattern detected, classify the intervals
            ClassifiedDates.Dates[0].IntervalSincePreviousDate = 0;
            ClassifiedDates.Dates[0].SmoothedInterval = 0;

            ClassifiedDates = CalculateIntervals(ClassifiedDates);
            ClassifiedDates = CalculateIntervalVariance(ClassifiedDates);
            ClassifiedDates.DetectedPattern = DateCollection.DatePattern.Interval;
            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromStart(DateCollection ClassifiedDates)
        {
            ClassifiedDates = CalculateDateDistanceFromStartOfMonth(ClassifiedDates);
           
            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromEnd(DateCollection ClassifiedDates)
        {
            ClassifiedDates = CalculateDateDistanceFromEndOfMonth(ClassifiedDates);

            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromStartOfMonth(DateCollection ClassifiedDates)
        {

            ClassifiedDates.PeriodUnit = DateCollection.TimeUnit.Month;
            ClassifiedDates.DistanceUnit = DateCollection.TimeUnit.Day;

            foreach (var item in ClassifiedDates.Dates)
            {
                var startOfMonth = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, 1, 0, 0, 0);
                var CleanItemDay = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, item.TheDateTime.Day, 0, 0, 0);
                var distanceFromStart = CleanItemDay - startOfMonth;
                item.DistanceFromPeriodStart = distanceFromStart.TotalDays;
            }

            var MaxDistance = ClassifiedDates.Dates.Max(q => q.DistanceFromPeriodStart);
            var MinDistance = ClassifiedDates.Dates.Min(q => q.DistanceFromPeriodStart);
            var AvgDistance = ClassifiedDates.Dates.Average(q => q.DistanceFromPeriodStart);
            var Variance = MaxDistance - MinDistance;
            ClassifiedDates.MeanDeviationFromStart = Variance;
            ClassifiedDates.AvgDistanceFromStart = AvgDistance + 1;

            if (ClassifiedDates.MeanDeviationFromStart < 0.02)
            {
                ClassifiedDates.DetectedPattern = DateCollection.DatePattern.StartOfMonth;
            }

            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromEndOfMonth(DateCollection ClassifiedDates)
        {

            ClassifiedDates.PeriodUnit = DateCollection.TimeUnit.Month;
            ClassifiedDates.DistanceUnit = DateCollection.TimeUnit.Day;

            foreach (var item in ClassifiedDates.Dates)
            {
                var startOfNextMonth = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, 1, 0, 0, 0).AddMonths(1);
                var lastDayOfThisMonth = startOfNextMonth.AddDays(-1);

                var CleanItemDay = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, item.TheDateTime.Day, 0, 0, 0);
                var distanceFromEnd = lastDayOfThisMonth -  CleanItemDay;
                item.DistanceFromPeriodEnd = distanceFromEnd.TotalDays;
            }

            var MaxDistance = ClassifiedDates.Dates.Max(q => q.DistanceFromPeriodEnd);
            var MinDistance = ClassifiedDates.Dates.Min(q => q.DistanceFromPeriodEnd);
            var AvgDistance = ClassifiedDates.Dates.Average(q => q.DistanceFromPeriodEnd);
            var Variance = MaxDistance - MinDistance;
            ClassifiedDates.MeanDeviationFromEnd = Variance;
            ClassifiedDates.AvgDistanceFromEnd = AvgDistance;


            if (ClassifiedDates.MeanDeviationFromEnd < 0.02)
            {
                ClassifiedDates.DetectedPattern = DateCollection.DatePattern.EndOfMonth;
            }

            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromNthWeekday(DateCollection ClassifiedDates)
        {

        

            var ItemsPerMonth = ClassifiedDates.Dates.GroupBy(info => info.TheDateTime.Month)
                .Select(group => new
                {
                    Metric = group.Key,
                    Count = group.Count()
                })
            .OrderBy(x => x.Metric);
            var AvgItemsPerMonth = ItemsPerMonth.Average(q => q.Count);
            if (AvgItemsPerMonth < 3) // We only cater for 2 entries per month in this pattern. if there are more, do not consider this pattern.
            {
                ClassifiedDates.PeriodUnit = DateCollection.TimeUnit.Month;
                ClassifiedDates.DistanceUnit = DateCollection.TimeUnit.WeekDay;

                foreach (var item in ClassifiedDates.Dates)
                {
                    var weekday = item.TheDateTime.DayOfWeek;
                    var monthday = item.TheDateTime.Day;
                    var NthCounter = 0;
                    for (int i = 1; i < item.TheDateTime.Day + 1; i++)
                    {
                        var ThatDay = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, i, 0, 0, 0);
                        if (ThatDay.DayOfWeek == weekday)
                        {
                            NthCounter += 1;
                        }
                    }

                    item.NthWeekday = $"{NthCounter}-{weekday.ToString()}";
                }

                // Detect whether there is a pattern
                var CheckForPattern = ClassifiedDates.Dates.GroupBy(info => info.NthWeekday)
                .Select(group => new
                {
                    Metric = group.Key,
                    Count = group.Count()
                }).OrderBy(x => x.Metric);
                if (AvgItemsPerMonth == CheckForPattern.Count())
                {
                    // Yes, pattern detected.
                    ClassifiedDates.DetectedPattern = DateCollection.DatePattern.EveryNthWeekDayOfMonth;
                }

                
            }

            

            return ClassifiedDates;
        }

        private DateCollection CalculateDateDistanceFromLastNthWeekday(DateCollection ClassifiedDates)
        {



            var ItemsPerMonth = ClassifiedDates.Dates.GroupBy(info => info.TheDateTime.Month)
                .Select(group => new
                {
                    Metric = group.Key,
                    Count = group.Count()
                })
            .OrderBy(x => x.Metric);

            if (ItemsPerMonth.Average(q => q.Count) < 3) // We only cater for 2 entries per month in this pattern. if there are more, do not consider this pattern.
            {
                ClassifiedDates.PeriodUnit = DateCollection.TimeUnit.Month;
                ClassifiedDates.DistanceUnit = DateCollection.TimeUnit.WeekDay;

                

                foreach (var item in ClassifiedDates.Dates)
                {
                    var weekday = item.TheDateTime.DayOfWeek;
                    var monthday = item.TheDateTime.Day;
                    var FirstDayOfMonth = item.TheDateTime.Subtract(new TimeSpan(item.TheDateTime.Day - 1, 0, 0, 0));
                    var NthCounter = 0;
                    for (int i = DateTime.DaysInMonth(FirstDayOfMonth.Year, FirstDayOfMonth.Month); i >= monthday; i--)
                    {
                        var ThatDay = new DateTime(item.TheDateTime.Year, item.TheDateTime.Month, i, 0, 0, 0);
                        if (ThatDay.DayOfWeek == weekday)
                        {
                            NthCounter += 1;
                        }
                    }

                    item.NthWeekday = $"{NthCounter}-last-{weekday.ToString()}";
                }

                ClassifiedDates.DetectedPattern = DateCollection.DatePattern.LastNthWeekDayOfMonth;
            }



            return ClassifiedDates;
        }


        private DateCollection CalculateIntervals(DateCollection input)
        {
            for (int i = 1; i < input.Dates.Count; i++)
            {
                var span = input.Dates[i].TheDateTime - input.Dates[i - 1].TheDateTime;
                input.Dates[i].IntervalSincePreviousDate = Convert.ToInt32(span.TotalMinutes);
            }
            return input;
        }

        private DateCollection CalculateIntervalVariance(DateCollection input)
        {
            if (input.Dates.Count < 2)
            {
                return input;
            }

            var GroupSimilarityThreshold = System.Double.Parse(Configuration["GroupSimilarityThreshold"]);

            var BucketOfIntervals = new List<double>();

            char Alpha = 'A';
            var FriendlyLetterIndex = (int)Alpha;



            for (int i = 1; i < input.Dates.Count; i++)
            {
                var span = input.Dates[i].IntervalSincePreviousDate - input.Dates[i - 1].IntervalSincePreviousDate;
                input.Dates[i].IntervalVariance = span;

                input.Dates[i].IntervalVariancePercentageOfInterval = (double)input.Dates[i].IntervalVariance / (double)input.Dates[i].IntervalSincePreviousDate;
            }


            //Calc Average Intervals
            var Outliers = new List<DateModel> ();
            input.Dates[1].StepLetter = Convert.ToChar(FriendlyLetterIndex);
            FriendlyLetterIndex += 1;
            Outliers.Add(input.Dates[1]);

            foreach (var item in input.Dates)
            {
                if (item.IntervalSincePreviousDate != 0)
                {
                    

                    if (!Outliers.Any(Q => Math.Abs(1 - ((double)Q.IntervalSincePreviousDate / (double)item.IntervalSincePreviousDate)) < GroupSimilarityThreshold))
                    {
                        item.StepLetter = Convert.ToChar(FriendlyLetterIndex);
                        FriendlyLetterIndex += 1;
                        Outliers.Add(item);
                    }

                }


            }

            foreach (var outlier in Outliers)
            {
                var closeNeighbours = input.Dates.Where(q => Math.Abs(1 - ((double)q.IntervalSincePreviousDate / (double)outlier.IntervalSincePreviousDate)) < GroupSimilarityThreshold);
                var cnSI = (int)closeNeighbours.Average(q => q.IntervalSincePreviousDate);
                var md = closeNeighbours.Max(q => Math.Abs(q.IntervalSincePreviousDate - cnSI));
                foreach (var cn in closeNeighbours)
                {
                    cn.SmoothedInterval = cnSI ;
                    cn.MaxDeviation = md;
                    cn.StepLetter = outlier.StepLetter;
                }
            }

            return input;
          
        }

        public System.DateTime GetNthWeekdayOfMonth(DateTime D, DayOfWeek weekDay, int N)
        {
            DateTime FoundDate = D;
            bool found = false;
            var FirstDayOfMonth = D.Subtract(new TimeSpan(D.Day - 1, 0, 0, 0));
            var TheDayOfweek = FirstDayOfMonth.DayOfWeek;

            //var weekday = D.DayOfWeek;
            var monthday = D.Day;
            var NthCounter = 0;
            for (int i = 1; i < DateTime.DaysInMonth(FirstDayOfMonth.Year, FirstDayOfMonth.Month) + 1; i++)
            {
                var ThatDay = new DateTime(D.Year, D.Month, i, D.Hour, D.Minute, D.Second);
                if (ThatDay.DayOfWeek == weekDay)
                {
                    NthCounter += 1;
                }

                if (NthCounter == N)
                {
                    FoundDate = ThatDay;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                return FoundDate;

            }
            else
            {
                throw new KeyNotFoundException("No such day");
            }

        }

        public System.DateTime GetLastNthWeekdayOfMonth(DateTime D, DayOfWeek weekDay, int N)
        {
            DateTime FoundDate = D;
            bool found = false;
            var FirstDayOfMonth = D.Subtract(new TimeSpan(D.Day - 1, 0, 0, 0));
            var TheDayOfweek = FirstDayOfMonth.DayOfWeek;

            //var weekday = D.DayOfWeek;
            var monthday = D.Day;
            var NthCounter = 0;
            for (int i = DateTime.DaysInMonth(FirstDayOfMonth.Year, FirstDayOfMonth.Month); i >=  1; i--)
            {
                var ThatDay = new DateTime(D.Year, D.Month, i, D.Hour, D.Minute, D.Second);
                if (ThatDay.DayOfWeek == weekDay)
                {
                    NthCounter += 1;
                }

                if (NthCounter == N)
                {
                    FoundDate = ThatDay;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                return FoundDate;

            }
            else
            {
                throw new KeyNotFoundException("No such day");
            }

        }



    }

    public class DateModel
    {
 
        public DateTime TheDateTime { get; set; }

        public int IntervalSincePreviousDate { get; set; }

        public int SmoothedInterval { get; set; }

        public double IntervalVariance { get; set; }

        public double IntervalVariancePercentageOfInterval { get; set; }

        public char StepLetter { get; internal set; }
        public int MaxDeviation { get; internal set; }

        /// <summary>
        /// How many Units since Period started? 
        /// e.g. 
        ///     3 days since month started
        ///     second Monday since month started
        ///     first friday of the month
        ///     second monday of the month
        /// </summary>
        public double DistanceFromPeriodStart { get; set; }


        /// <summary>
        /// How many Units before Period ends? 
        /// e.g. 
        ///     second last day of month    -- Distance from end
        ///     last friday of the month  -- Distance from end
        /// </summary>
        public double DistanceFromPeriodEnd { get; set; }
        public string NthWeekday { get; internal set; }
    }

    public class DateCollection
    {
        public DateCollection()
        {
            this.Dates = new List<DateModel>();
        }

        public List<DateModel> Dates { get; set; }

        public double MeanDeviationFromStart { get; set; }

        public double AvgDistanceFromStart { get; set; }

        public double AvgDeviationToEnd { get; set; }

        public DatePattern DetectedPattern { get; set; }

        public TimeUnit DistanceUnit { get; set; }

        public TimeUnit PeriodUnit { get; set; }
        public double MeanDeviationFromEnd { get; internal set; }
        public double AvgDistanceFromEnd { get; internal set; }

        public enum TimeUnit
        {
            Minute,
            Hour,
            Day,
            Week,
            Month,
            Quarter,
            Year,
            WeekDay
        }

        public enum DatePattern
        {
            Interval,
            StartOfMonth,
            EndOfMonth,
            EveryNthWeekDayOfMonth,
            LastNthWeekDayOfMonth
        }
    }

    public static class DateTimeExtensions
    {
        ///<summary>Gets the first week day following a date.</summary>
        ///<param name="date">The date.</param>
        ///<param name="dayOfWeek">The day of week to return.</param>
        ///<returns>The first dayOfWeek day following date, or date if it is on dayOfWeek.</returns>
        public static DateTime Next(this DateTime date, DayOfWeek dayOfWeek)
        {
            return date.AddDays((dayOfWeek < date.DayOfWeek ? 7 : 0) + dayOfWeek - date.DayOfWeek);
        }
    }
}
