﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using static GapLearnerLib.GrammarLearner.GeneticResult;

namespace GapLearnerLib
{

    public class GrammarLearner
    {
        public static IConfigurationRoot Configuration { get; set; }
        public GrammarLearner()
        {
            var configTest = new LearnerConfig();
            Configuration = configTest.LoadConfig();
        }

        public static GeneticResult InferGeneticPattern_Basic(List<Letter> input)
        {


            var ListOfResults = new List<GeneticResult>();

            for (int i = 0; i < input.Count; i++)
            {
                var testPattern = new List<Letter>();
                for (int j = 0; j < i + 1; j++)
                {
                    testPattern.Add(input[j]);
                }

                ListOfResults.Add(ScorePattern(input, testPattern));

            }

            var BestPattern = RankPatterns(ListOfResults).First();

            return BestPattern;

        }

        public GeneticResult InferGeneticPattern_Evolution(List<Letter> input)
        {

            var FinalListOfResults = new List<GeneticResult>();

            input = RemoveLettersStatisticalAnomalies(input);

            var maxLengthOfPattern = System.Int32.Parse(Configuration["OnlyConsiderLastXItems"]);
            var maxLenghtConsidered = maxLengthOfPattern;
            if (input.Count < maxLengthOfPattern)
            {
                maxLenghtConsidered = input.Count;
            }
            double MostEfficientPatternSoFar = 0;
            int maxDepth = System.Int32.Parse(Configuration["maxIterations"]);
            double EfficiencyTarget = System.Double.Parse(Configuration["EfficiencyTarget"]);
            int StartIndex = input.Count - maxLenghtConsidered;
            var ConsideredInput = input.Skip(StartIndex).ToList();
            int currentDepth = 0;

            //var testResults = new List<GeneticResult>();
            List<GeneticResult> CurrentDepthPatterns = new List<GeneticResult>();
            var CurrentGenerationResults = new List<GeneticResult>();

            GeneticResult BestPattern;

            while (MostEfficientPatternSoFar < EfficiencyTarget && currentDepth <= maxDepth) 
            {
                currentDepth += 1;
                
                CurrentDepthPatterns = GetPatternsForThisDepth(ConsideredInput, CurrentGenerationResults);
                CurrentGenerationResults.Clear();
                CurrentDepthPatterns.ForEach(p =>
                {
                    CurrentGenerationResults.Add(ScorePattern(input, p.Pattern));
                });

                CurrentGenerationResults = RankPatterns(CurrentGenerationResults).OrderByDescending(q => q.Effectiveness).Take(System.Int32.Parse(Configuration["SurvivorsToEvolve"])).ToList(); 
                BestPattern = RankPatterns(CurrentGenerationResults).FirstOrDefault();

                foreach (var item in CurrentGenerationResults)
                {
                    if (!FinalListOfResults.Any(q => string.Join(",", q.Pattern).ToLower() == string.Join(",", item.Pattern).ToLower())) 
                    {
                        FinalListOfResults.Add(item);
                    }
                }

                if (BestPattern != null)
                {
                    MostEfficientPatternSoFar = BestPattern.Effectiveness;
                }
            }

            // Now validate it against the most recent entries
            double LastXPercentLettersMoreSignificant = System.Double.Parse(Configuration["LastXPercentLettersMoreSignificant"]);
            if (LastXPercentLettersMoreSignificant > 0)
            {

                int LastXPercStartingIndex = (int)Math.Floor(input.Count * (1 - LastXPercentLettersMoreSignificant));
                var LastXPercInput = input.Skip(LastXPercStartingIndex).ToList();
                var LastXPercResults = new List<GeneticResult>();
                CurrentGenerationResults.Clear();
                currentDepth = 0;
                MostEfficientPatternSoFar = 0;

                Console.WriteLine();
                Console.WriteLine($" Considering the last {LastXPercentLettersMoreSignificant} of events more significant: {string.Join(",", LastXPercInput)}");


                while (MostEfficientPatternSoFar < EfficiencyTarget && currentDepth <= maxDepth)
                {
                    currentDepth += 1;

                    CurrentDepthPatterns = GetPatternsForThisDepth(LastXPercInput, CurrentGenerationResults);
                    CurrentGenerationResults.Clear();
                    CurrentDepthPatterns.ForEach(p =>
                    {
                        CurrentGenerationResults.Add(ScorePattern(LastXPercInput, p.Pattern)); // do not score against entire pattern, only last X perc
                    });

                    CurrentGenerationResults = RankPatterns(CurrentGenerationResults).OrderByDescending(q => q.Effectiveness).Take(System.Int32.Parse(Configuration["SurvivorsToEvolve"])).ToList(); // TODO: Make the number of survivors (e.g. 10) configurable
                    BestPattern = RankPatterns(CurrentGenerationResults).FirstOrDefault();

                    foreach (var item in CurrentGenerationResults)
                    {
                        if (!LastXPercResults.Any(q => string.Join(",", q.Pattern).ToLower() == string.Join(",", item.Pattern).ToLower()))
                        {
                            LastXPercResults.Add(item);
                        }
                    }

                    if (BestPattern != null)
                    {
                        MostEfficientPatternSoFar = BestPattern.Effectiveness;
                    }
                }


                var LastXPercRanked = RankPatterns(LastXPercResults);

                foreach (var lastXResult in LastXPercRanked.OrderByDescending(q => q.Effectiveness).Take(5).ToList())
                {
                    var correspondingFinalist = FinalListOfResults.Find(q => string.Join(",", q.Pattern) == string.Join(",", lastXResult.Pattern));
                    if (correspondingFinalist != null) // 
                    {
                        if (correspondingFinalist.Effectiveness < lastXResult.Effectiveness * System.Double.Parse(Configuration["BoostingFactor"]))
                        {
                            correspondingFinalist.Effectiveness = lastXResult.Effectiveness * System.Double.Parse(Configuration["BoostingFactor"]); // Boost the effectiveness
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine($"Boosting {string.Join(",", lastXResult.Pattern)} to {lastXResult.Effectiveness * System.Double.Parse(Configuration["BoostingFactor"])} ");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        
                    }
                    else
                    {
                       
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine($"Recovering culled pattern. Boosting {string.Join(",", lastXResult.Pattern)} to {lastXResult.Effectiveness * System.Double.Parse(Configuration["BoostingFactor"])} ");
                        Console.ForegroundColor = ConsoleColor.Gray;

                        lastXResult.Effectiveness *= System.Double.Parse(Configuration["BoostingFactor"]);
                        FinalListOfResults.Add(lastXResult);

                    }

                }
            }
          

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            foreach (var finalist in FinalListOfResults.OrderByDescending(q => q.Effectiveness).ToList())
            {
                Console.WriteLine($" Pattern: {string.Join(",", finalist.Pattern)} Ef:{finalist.Effectiveness}  Errors:{finalist.ErrorCount} ErrRate:{finalist.ErrorRate} Score:{finalist.Score}" );
            }
            Console.ForegroundColor = ConsoleColor.Gray;




            BestPattern = FinalListOfResults.OrderByDescending(q => q.Effectiveness).First();

            return BestPattern;

        }

        private List<GeneticResult> GetPatternsForThisDepth(List<Letter> consideredInput, List<GeneticResult> CurrentPatternsConsidered)
        {
            var result = new List<GeneticResult>();

            int currentLenghth = CurrentPatternsConsidered.Count + 1;

            if (CurrentPatternsConsidered.Count == 0)
            {
                // Start with all new pattern, one for each distinct letter found in the input
                
                var distinctLetters = consideredInput
                                          .GroupBy(p => p.PatternLetter)
                                          .Select(g => g.First())
                                          .ToList();

                distinctLetters.ForEach(q =>
                {
                    result.Add(new GeneticResult() { Pattern = new List<Letter>() { q } });
                });

            }
            else
            {
                // Here we must "evolve", but finding the "next" letter after what is already there... but finding "random" variations of future letters to skip - up to the lenghth determined by the current depth.
                
                foreach (var currentPattern in CurrentPatternsConsidered)
                {
                    
                    var LastLetter = currentPattern.Pattern.Last();

                    var AddThese = FindNextNLettersFollowingX(consideredInput, LastLetter, Int32.Parse(Configuration["LookAheadNextNLetters"]));  // This is where we "evolve", adding those letters known to follow what we have to far, but skipping N number of letters that could be noise.

                    foreach (var item in AddThese)
                    {
                        var newPattern = new GeneticResult() { Pattern = new List<Letter>(), OriginalInputLength = currentPattern.OriginalInputLength };
                        foreach (var p in currentPattern.Pattern)
                        {
                            newPattern.Pattern.Add(p);
                        }

                        newPattern.Pattern.Add(item);

                        if (!result.Any(q => string.Join(",", q.Pattern).ToLower() == string.Join(",", newPattern.Pattern).ToLower()));
                        {
                            result.Add(newPattern);
                        }

                       
                    }

                }

            }

            var distinctResults = result
                                          .GroupBy(p => string.Join(",", p.Pattern))
                                          .Select(g => g.First())
                                          .ToList();
            return distinctResults;
        }

        private List<Letter> RemoveLettersStatisticalAnomalies(List<Letter> input)
        {
            var result = input.GroupBy(info => info.PatternLetter)
                            .Select(group => new
                            {
                                Metric = group.Key,
                                Count = group.Count()
                            })
                        .OrderBy(x => x.Metric);

            foreach (var m in result)
            {
                double StatisticalAnomalies = System.Double.Parse(Configuration["IgnoreStatisticalAnomalies"]);
                var perc = (double)m.Count / (double)input.Count;
                if (perc < StatisticalAnomalies)
                {
                    // Not statistically significant
                    input.RemoveAll(q => q.PatternLetter == m.Metric);
                }
            }

            return input;
        }

        public static List<Letter> FindNextNLettersFollowingX(List<Letter> input, Letter x, int NextNLetters)
        {
            var result = new List<Letter>();


            for (int i = 0; i < input.Count; i++)
            {
                if (input[i].PatternLetter.ToLower() == x.PatternLetter.ToLower())
                {
                    // Found the letter. Now find next N letters following this.
                    for (int j = i + 1; j <= i + NextNLetters; j++)
                    {
                        if (j <= input.Count - 1) //make sure we dont go over the boundary
                        {
                            result.Add(input[j]);
                        }
                    }
                }
            }

            return result.DistinctBy(q => q.PatternLetter).ToList();

        }


        /// <summary>
        /// Scoring of Evolutionary scores.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="testPattern"></param>
        /// <returns></returns>
        private static GeneticResult ScorePattern(List<Letter> input, List<Letter> testPattern)
        {
            var Result = new GeneticResult() { ErrorCount = 0, Score = 0, Pattern = testPattern, OriginalInputLength = input.Count };
            var j = 0;

            double LastXPercentLettersMoreSignificant = System.Double.Parse(Configuration["LastXPercentLettersMoreSignificant"]);

            var PositionalScoring = new Dictionary<int, double>();

            for (int i = 0; i < input.Count; i++)
            {
                if (input[i].PatternLetter.ToLower() == testPattern[j].PatternLetter.ToLower())
                {
                    // match
                    Result.Score += 1;

                    if (PositionalScoring.ContainsKey(j))
                    {
                        PositionalScoring[j] += 1;
                    }
                    else
                    {
                        PositionalScoring.Add(j, 1);
                    }


                    j += 1;

                }
                else
                {
                    // no match. Count this as an error, and do not move to the next letter in the test pattern.
                    Result.ErrorCount += 1;
                    if (PositionalScoring.ContainsKey(j))
                    {
                        PositionalScoring[j] -= 1;
                    }
                    else
                    {
                        PositionalScoring.Add(j, 0);
                    }

                    // Peek ahead, to see if we should move the the next letter in the test pattern
                    try
                    {
                        if (input[i + 1].PatternLetter.ToLower() == testPattern[j].PatternLetter.ToLower())
                        {
                           // Skip a letter (do not advance J)
                        }
                        else
                        {
                            if (input[i + 1].PatternLetter.ToLower() == testPattern[j + 1].PatternLetter.ToLower())
                            {
                                Result.ErrorCount += 0.1;
                                j += 1; // Advance 1
                            }
                            else if (input[i + 1].PatternLetter.ToLower() == testPattern[j + 2].PatternLetter.ToLower())
                            {
                                Result.ErrorCount += 0.2;
                                j += 2; // Advance 2
                            }
                            else if (input[i + 1].PatternLetter.ToLower() == testPattern[j + 3].PatternLetter.ToLower())
                            {
                                Result.ErrorCount += 0.3;
                                j += 3; // Advance 3
                            }
                            else if (input[i + 1].PatternLetter.ToLower() == testPattern[j + 4].PatternLetter.ToLower())
                            {
                                Result.ErrorCount += 0.4;
                                j += 4; // Advance 4
                            }
                            else
                            {
                                Result.ErrorCount += 0.5;
                                j += 5; // Advance 5
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }


                }

                if (testPattern.Count == j)
                {
                    j = 0;
                }

                Result.Next = testPattern[j];
            }

            double TotalScore = 0;
            foreach (var item in PositionalScoring.Values)
            {
                TotalScore += item;
            }
            Result.Score = Convert.ToInt16(((double)PositionalScoring.Values.Sum() / (double)PositionalScoring.Values.Count) * 100);


            return Result;
        }

        private static List<GeneticResult> RankPatterns(List<GeneticResult> input)
        {

            // Even though Positional Learning seems promising, the algorith below (which does not use the positionally stenghthened score, but rather rewards compactness and takes the error rate into consideration - which has better results.
            // The formulate below prevents overfitting by rewarding compactness over a lack of errors / perfect fit - giving us a more balanced Effectiveness score.

            foreach (var item in input)
            {
                var ErrorRate = (double)item.ErrorCount / (double)item.OriginalInputLength;
                double Compactness = 1 - ((double)item.Pattern.Count / (double)item.OriginalInputLength);
                var Effectiveness = (Compactness * (1 - ErrorRate)) - ErrorRate;
                item.Effectiveness = Effectiveness;
                item.ErrorRate = ErrorRate;

                //item.Effectiveness = item.Effectiveness * (1 + item.BoostLaterSignificance);
                item.Effectiveness += item.BoostLaterSignificance;
            }

            var ranked = input.OrderByDescending(q => q.Effectiveness);

            //Console.WriteLine("Ranked Scores:");
            //foreach (var item in ranked)
            //{
            //    Console.WriteLine(string.Join(",",item.Pattern));
            //    Console.WriteLine($"   Score: {item.Score}   Lenght: {item.Pattern.Count}    Errors: {item.ErrorCount}  Effectiveness: {item.Effectiveness}");
            //    Console.WriteLine();
            //}

            return ranked.ToList();
        }


        public class GeneticResult
        {
            [DebuggerDisplay(@"Pattern = {String.Join("","", Pattern)}")]
            public List<Letter> Pattern { get; set; }

            public double ErrorCount { get; set; }

            public double BoostLaterSignificance { get; set; }

            public double Score { get; set; }

            public int OriginalInputLength { get; set; }

            public double ErrorRate { get; set; }

            public double Effectiveness { get; set; }

            public Letter Next { get; set; }

            public string NextLetter { get; set; }

            /// <summary>
            /// This Pattern is no longer deemed viable (it is killed)
            /// </summary>
            public bool Culled { get; set; }

            [DebuggerDisplay("PatternLetter")]
            public class Letter
            {
                public string PatternLetter { get; set; }
                public string LetterPayload { get; set; }
                public double MaxDeviation { get; set; }

                public override string ToString()
                {
                    return string.Format("{0}", PatternLetter);
                }
            }


        }

    }

    public static class GrammarExtensions
    {

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

    }

}
