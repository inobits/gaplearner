﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GapLearnerLib.GrammarLearner.GeneticResult;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();


            //LearnTest();

            MonFriTest();
            //TestWithHours(5);  // TODO: Gets inefficient with fewer than 2-3 hours. 1 hours is accurate but SLOW
            //TestWithMonthlyPattern_FirstMondayEveryMonth();
            //TestWithMonthlyPattern_LastMondayEveryMonth();
            //TestWithMonthlyPattern_FirstXDayEveryMonth(3);
            //TestWithMonthlyPattern_LastXDayEveryMonth(1);

            sw.Stop();

            Console.WriteLine();
            Console.WriteLine("Elapsed time: " + sw.ElapsedMilliseconds.ToString());
            Console.ReadLine();

        }




        static void MonFriTest()
        {
            var rnd = new Random();
            var dateList = GetSetString("Date");
            var set1List = GetSet("MonFri");
            var eventDates = new List<DateTime>();

            for (int i = 0; i < set1List.Count; i++)
            {
                if (set1List[i] > 0)
                {
                    var theDateString = dateList[i];
                    var theDate = DateTime.Parse(theDateString);
                    int randomAddedMins = rnd.Next(180) - 90;
                    theDate = theDate.AddHours(5);
                    theDate = theDate.AddMinutes(randomAddedMins);
                    eventDates.Add(theDate);
                }
            }

            StandardDateLearner(eventDates);


        }

        static void TestWithHours(int NumberOfHours)
        {
            var rnd = new Random();
            var eventDates = new List<DateTime>();
            DateTime theDateString = DateTime.Now;

            for (int i = 0; i < 100; i++)
            {
                theDateString = theDateString.AddHours(NumberOfHours);
                    int randomAddedMins = rnd.Next(10) - 5;
                    theDateString = theDateString.AddMinutes(randomAddedMins);
                    
                    eventDates.Add(theDateString);
                
            }

            StandardDateLearner(eventDates);


        }

        static void TestWithMonthlyPattern_FirstMondayEveryMonth()
        {
            var rnd = new Random();
            var eventDates = new List<DateTime>();
            DateTime theDateString = new DateTime(2017,1,1);

            var t2 = theDateString;

            for (int i = 0; i < 5; i++)
            {
                var t = theDateString.AddMonths(i);
                try
                {
                    t = new GapLearnerLib.DateModeler().GetNthWeekdayOfMonth(t, DayOfWeek.Monday, 1);
                    //int randomAddedMins = rnd.Next(10) - 5;
                    //t = t.AddMinutes(randomAddedMins);

                    eventDates.Add(t);
                }
                catch (KeyNotFoundException)
                {

                }
               

                t2 = theDateString.AddMonths(i);
                try
                {
                    t2 = new GapLearnerLib.DateModeler().GetNthWeekdayOfMonth(t2, DayOfWeek.Friday, 2);
                    //int randomAddedMins = rnd.Next(10) - 5;
                    //t = t.AddMinutes(randomAddedMins);

                    eventDates.Add(t2);
                }
                catch (KeyNotFoundException)
                {

                }


            }

            StandardDateLearner(eventDates);


        }

        static void TestWithMonthlyPattern_LastMondayEveryMonth()
        {
            var rnd = new Random();
            var eventDates = new List<DateTime>();
            DateTime theDateString = new DateTime(2017, 1, 1);

            var t2 = theDateString;

            for (int i = 0; i < 5; i++)
            {
                var t = theDateString.AddMonths(i);
                try
                {
                    t = new GapLearnerLib.DateModeler().GetLastNthWeekdayOfMonth(t, DayOfWeek.Monday, 2);
                    //int randomAddedMins = rnd.Next(10) - 5;
                    //t = t.AddMinutes(randomAddedMins);

                    eventDates.Add(t);
                }
                catch (KeyNotFoundException)
                {

                }


                t2 = theDateString.AddMonths(i);
                try
                {
                    t2 = new GapLearnerLib.DateModeler().GetLastNthWeekdayOfMonth(t2, DayOfWeek.Friday, 1);
                    //int randomAddedMins = rnd.Next(10) - 5;
                    //t = t.AddMinutes(randomAddedMins);

                    eventDates.Add(t2);
                }
                catch (KeyNotFoundException)
                {

                }


            }

            StandardDateLearner(eventDates);


        }


        static void TestWithMonthlyPattern_FirstXDayEveryMonth(int x)
        {
            var rnd = new Random();
            var eventDates = new List<DateTime>();
            DateTime theDateString = new DateTime(2016, 11, 14, 14, 0, 0);

           

            for (int i = 0; i < 5; i++)
            {
                //theDateString = DateTime.Now;
                var t = theDateString.AddMonths(i);
                t = t.Subtract(new TimeSpan(t.Day - 1, 0, 0, 0));
                t = t.AddDays(x);
                //t = new GapLearnerLib.DateModeler().GetFirstWeekdayOfMonth(t, DayOfWeek.Tuesday);
                int randomAddedMins = rnd.Next(10) - 5;
                t = t.AddMinutes(randomAddedMins);

                eventDates.Add(t);

            }

            StandardDateLearner(eventDates);


        }

        static void TestWithMonthlyPattern_LastXDayEveryMonth(int x)
        {
            var rnd = new Random();
            var eventDates = new List<DateTime>();
            DateTime theDateString = new DateTime(2017, 9, 8, 11, 0, 0);



            for (int i = 0; i < 6; i++)
            {
                //theDateString = DateTime.Now;
                var t = theDateString.AddMonths(i);
                t = t.Subtract(new TimeSpan(t.Day - 1, 0, 0, 0));
                t = t.AddDays(x * -1);
                //t = new GapLearnerLib.DateModeler().GetFirstWeekdayOfMonth(t, DayOfWeek.Tuesday);
                int randomAddedMins = rnd.Next(10) - 5;
                t = t.AddMinutes(randomAddedMins);

                eventDates.Add(t);

            }


            StandardDateLearner(eventDates);



        }

        static void StandardDateLearner(List<DateTime> eventDates)
        {
            var ConsiredDates = eventDates.Take(150).ToList();
            var ClassifiedDates = new GapLearnerLib.DateModeler().ClassifyDates(ConsiredDates);

            switch (ClassifiedDates.DetectedPattern)
            {
                case GapLearnerLib.DateCollection.DatePattern.Interval:
                    Console.WriteLine("Interval based pattern...");
                    Console.WriteLine("Analysing pattern: " + string.Join(",", ClassifiedDates.Dates.Skip(1).Select(q => q.StepLetter.ToString()).ToList()));

                    // Here we do the magic
                    var BestPattern = new GapLearnerLib.GrammarLearner().InferGeneticPattern_Evolution(ClassifiedDates.Dates.Skip(1).Select(q => new GapLearnerLib.GrammarLearner.GeneticResult.Letter() { PatternLetter = q.StepLetter.ToString(), LetterPayload = q.SmoothedInterval.ToString(), MaxDeviation = q.MaxDeviation }).ToList());


                    // What pattern did we find?
                    Console.WriteLine($"Found Interval Pattern: {string.Join(",", BestPattern.Pattern.Select(q => q.PatternLetter))}   ErrorRate: {BestPattern.ErrorRate.ToString("P2")}  -  Score:{BestPattern.Score}   Next:{BestPattern.Next.PatternLetter}");
                    Console.WriteLine();
                    // When should the next event be?
                    var NextDate = ConsiredDates.Last().AddMinutes(double.Parse(BestPattern.Next.LetterPayload) * 1);
                    Console.WriteLine($"Last event observed on {ConsiredDates.Last().ToString()}. Next event expected around {NextDate.ToString()}, with a maximum observed variance of {BestPattern.Next.MaxDeviation} minutes");

                    break;
                case GapLearnerLib.DateCollection.DatePattern.EveryNthWeekDayOfMonth:
                    Console.WriteLine("Nth Weekday Interval based pattern...");
                    Console.WriteLine("Analysing pattern: " + string.Join(",", ClassifiedDates.Dates.Select(q => q.NthWeekday.ToString()).ToList()));

                    // Here we do the magic
                    var BestWDPattern = new GapLearnerLib.GrammarLearner().InferGeneticPattern_Evolution(ClassifiedDates.Dates.Select(q => new GapLearnerLib.GrammarLearner.GeneticResult.Letter() { PatternLetter = q.NthWeekday, LetterPayload = q.NthWeekday, MaxDeviation = 0 }).ToList());

                    if (BestWDPattern.ErrorRate > 0.4)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"  Error rate very high : {BestWDPattern.ErrorRate}");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }

                    // What pattern did we find?
                    Console.WriteLine($"Found Nth Weekday Interval Pattern: {string.Join(",", BestWDPattern.Pattern.Select(q => q.PatternLetter))}   ErrorRate: {BestWDPattern.ErrorRate.ToString("P2")}  -  Score:{BestWDPattern.Score}   Next:{BestWDPattern.Next.PatternLetter}");
                    Console.WriteLine();
                    // When should the next event be?
                    var NextWDMonth = ConsiredDates.Last().AddMonths(1);
                    var startOfMonth = new DateTime(NextWDMonth.Year, NextWDMonth.Month, 1, 0, 0, 0);
                    //DayOfWeek ThatWeekday; 
                    Enum.TryParse(BestWDPattern.Next.LetterPayload.Split('-')[1], out DayOfWeek ThatWeekday);

                    try
                    {
                        var NextWDDate = new GapLearnerLib.DateModeler().GetNthWeekdayOfMonth(NextWDMonth, ThatWeekday, int.Parse(BestWDPattern.Next.LetterPayload.Split('-')[0]));

                        Console.WriteLine($"Last event observed on {ConsiredDates.Last().ToString()}. Next event expected around {NextWDDate.ToString()}, with a maximum observed variance of {BestWDPattern.Next.MaxDeviation} minutes");

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("No next event could be predicted");
                    }
                  
                    break;
                case GapLearnerLib.DateCollection.DatePattern.LastNthWeekDayOfMonth:
                    Console.WriteLine("Last Nth Weekday Interval based pattern...");
                    Console.WriteLine("Analysing pattern: " + string.Join(",", ClassifiedDates.Dates.Select(q => q.NthWeekday.ToString()).ToList()));

                    // Here we do the magic
                    var BestLWDPattern = new GapLearnerLib.GrammarLearner().InferGeneticPattern_Evolution(ClassifiedDates.Dates.Select(q => new GapLearnerLib.GrammarLearner.GeneticResult.Letter() { PatternLetter = q.NthWeekday, LetterPayload = q.NthWeekday, MaxDeviation = 0 }).ToList());

                    if (BestLWDPattern.ErrorRate > 0.4)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"  Error rate very high : {BestLWDPattern.ErrorRate}");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }

                    // What pattern did we find?
                    Console.WriteLine($"Found Last Nth Weekday Interval Pattern: {string.Join(",", BestLWDPattern.Pattern.Select(q => q.PatternLetter))}   ErrorRate: {BestLWDPattern.ErrorRate.ToString("P2")}  -  Score:{BestLWDPattern.Score}   Next:{BestLWDPattern.Next.PatternLetter}");
                    Console.WriteLine();
                    // When should the next event be?
                    var NextLWDMonth = ConsiredDates.Last().AddMonths(1);
                    var startOfMonthL = new DateTime(NextLWDMonth.Year, NextLWDMonth.Month, 1, 0, 0, 0);
                    //DayOfWeek ThatWeekday; 
                    Enum.TryParse(BestLWDPattern.Next.LetterPayload.Split('-')[1], out DayOfWeek ThatLastWeekday);

                    try
                    {
                        var NextWDDate = new GapLearnerLib.DateModeler().GetLastNthWeekdayOfMonth(NextLWDMonth, ThatLastWeekday, int.Parse(BestLWDPattern.Next.LetterPayload.Split('-')[0]));

                        Console.WriteLine($"Last event observed on {ConsiredDates.Last().ToString()}. Next event expected around {NextWDDate.ToString()}, with a maximum observed variance of {BestLWDPattern.Next.MaxDeviation} minutes");

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("No next event could be predicted");
                    }

                    break;
                case GapLearnerLib.DateCollection.DatePattern.StartOfMonth:
                    Console.WriteLine($"Pattern detected: event happens every {ClassifiedDates.AvgDistanceFromStart} {ClassifiedDates.DistanceUnit.ToString()} of every {ClassifiedDates.PeriodUnit.ToString()}");
                    // When should the next event be?
                    var NextSMonth = ConsiredDates.Last().AddMonths(1);
                    var startOfMonth2 = new DateTime(NextSMonth.Year, NextSMonth.Month, 1, NextSMonth.Hour, NextSMonth.Minute, NextSMonth.Second);
                    var SMNextDate = startOfMonth2.AddDays(ClassifiedDates.AvgDistanceFromStart - 1);
                    Console.WriteLine($"Last event observed on {ConsiredDates.Last().ToString()}. Next event expected around {SMNextDate.ToString()}.");

                    break;
                case GapLearnerLib.DateCollection.DatePattern.EndOfMonth:

                    Console.WriteLine($"Pattern detected: event happens {ClassifiedDates.AvgDistanceFromEnd} {ClassifiedDates.DistanceUnit.ToString()} from the end of every {ClassifiedDates.PeriodUnit.ToString()}");

                    // When should the next event be?
                    var NextEMonth = ConsiredDates.Last().AddMonths(2);
                    var LastKnown = ClassifiedDates.Dates.First();
                    var EndOfEMonth2 = new DateTime(NextEMonth.Year, NextEMonth.Month, 1, LastKnown.TheDateTime.Hour, LastKnown.TheDateTime.Minute, LastKnown.TheDateTime.Second);
                    var EMNextDate = EndOfEMonth2.AddDays((ClassifiedDates.AvgDistanceFromEnd + 1) * -1);
                    Console.WriteLine($"Last event observed on {ConsiredDates.Last().ToString()}. Next event expected around {EMNextDate.ToString()}.");

                    break;
                default:
                    break;
            }

        }

        static void LearnTest()
        {
            var Inputs = new List<string>()
            {
                //"A,A,A,A,A,B,A"
                //"A,B,B,C,D,A,A,B,B,C,D,A,A,B,B,C,D,A,A,B,B,C,D,A",
                //"A,B,B,C,D,A,A,B,B,C,D,A,B,A,B,B,C,D,A,A,B,B,C,D,A",
                //"A,B,B,C,D,A,B,B,C,D,A,A,B,B,C,D,A,A,B,B,C,D,A"
                //"A,A,A,A,A",
                //"A,B,A,B,A,B,A,B",
                //"A,B,A,B,A,B,A,B,A,A,B,A,B",
                //"A,B,C,A,B,C,A,B,C,A,B",
                //"A,B,A,B,C,A,B,A,B,C,A,B,A,B,C,A,B",
                //"A,A,B,B,A,A,B,B,B,B,A,A,B,B,A",
                //"A,A,B,B,B,B,B,B,B,B,A,B,B,B,C,B,B,B,B,B,B,B,B",
                //"A,A,A,A,A,A,A,A,B,B,B,B,B,B",
                //"A,B,A,B,C,A,B,B,C,A,B,A,B,C,A,B,A",
                "Louis,Bob,Louis,Bob,Carina,Bob,Carina,Bob"
            };

            foreach (var item in Inputs)
            {
                Console.WriteLine(item);
                var input = item.Split(',').ToList();

                var BestPattern = new GapLearnerLib.GrammarLearner().InferGeneticPattern_Evolution(input.Select(q => new GapLearnerLib.GrammarLearner.GeneticResult.Letter() { PatternLetter = q, LetterPayload = q }).ToList());

                //var BestPattern = GapLearnerLib.GrammarLearner.InferGeneticPattern_Basic(ClassifiedDates.Skip(1).Select(q => new GapLearnerLib.GrammarLearner.GeneticResult.Letter() { PatternLetter = q.StepLetter.ToString(), LetterPayload = q.SmoothedInterval.ToString() }).ToList());

                Console.WriteLine($"Found Pattern: {string.Join(",", BestPattern.Pattern.Select(q => q.PatternLetter))}   ErrorRate: {BestPattern.ErrorRate.ToString("P2")}  -  Score:{BestPattern.Score}   Next:{BestPattern.Next.PatternLetter}");
                Console.WriteLine();


            }


        }


        static List<int> GetSet(string setName)
        {
            var results = new List<int>();
            using (ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(@"backup frequencies.xlsx")))
            {
                var sheet1 = pck.Workbook.Worksheets["Sheet1"];
                var t1 = sheet1.Tables["Table1"];

                foreach (var col in t1.Columns)
                {
                    if (col.Name.ToLower() == setName.ToLower())
                    {
                        Console.WriteLine(col.Name);
                        var i = 2;
                        var val = sheet1.Cells[i, col.Id];
                        while (val.Value != null)
                        {
                            if (int.TryParse(val.Value.ToString(), out int theInt))
                            {
                                results.Add(theInt);
                                i += 1;
                                val = sheet1.Cells[i, col.Id];

                            }

                        }

                    }
                }
            }
            return results;
        }

        static List<string> GetSetString(string setName)
        {
            var results = new List<string>();
            using (ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(@"backup frequencies.xlsx")))
            {
                var sheet1 = pck.Workbook.Worksheets["Sheet1"];
                var t1 = sheet1.Tables["Table1"];

                foreach (var col in t1.Columns)
                {
                    if (col.Name.ToLower() == setName.ToLower())
                    {
                        Console.WriteLine(col.Name);
                        var i = 2;
                        var val = sheet1.Cells[i, col.Id];
                        while (val.Value != null)
                        {
                            results.Add(val.Value.ToString());

                            i += 1;
                            val = sheet1.Cells[i, col.Id];

                        }

                    }
                }
            }
            return results;
        }



    }
}
